# Overview

This is the root-repository template used by Google Distributed Edge Consumer Edge solution.

NOTE: This is a template repository and needs to be forked, write access will not be granted for this repository.

## Installed by Default
- Longhorn
- CDI for KubeVirt
- KubeVirt VNC Lite (no-vnc)
- Default echo service (default namespace)
- External Secrets


## Local Validation

Assuming `nomos` is installed (via `gcloud components install nomos`)

```
nomos vet --no-api-server-check --path config/
```

### ACM Overview

See [our documentation](https://cloud.google.com/anthos-config-management/docs/repo) for how to use each subdirectory.
